
// Project: EASY_COM 
// Created: 2017-02-26

// show all errors
SetErrorMode(0)

// set window properties
SetWindowTitle( "EASY_COM" )
SetWindowSize( 1024, 768, 0 )

// set display properties
SetVirtualResolution( 1024, 768 )
SetOrientationAllowed( 1, 1, 1, 1 )
SetSyncRate( 30, 0 ) // 30fps instead of 60 to save battery
UseNewDefaultFonts( 1 ) // since version 2.0.22 we can use nicer default fonts

global net as integer
net = hostnetwork("VOU","PAD",4449)

do
    getMessage()
    sendMessage()
	print(GetNetworkNumClients(net))

    Sync()
loop

function getMessage() 
	msg = GetNetworkMessage(net)
	if msg > 0 
		addr$ = GetNetworkMessageString(msg)
		info_one$ = getnetworkmessagestring(msg)
		info_two$ = getnetworkmessagestring(msg)
		
		DeleteNetworkMessage(msg)
		if info_one$ <> ""
			if getfileexists("got.txt") = 1
				deletefile("got.txt")
			endif
			client = GetNetworkMessageFromClient(msg)
			f = opentowrite("got.txt")
			writestring(f, addr$)
			writestring(f, info_one$)
			writestring(f, info_two$)
			message(str(client))
			writestring(f, str(client))
			closefile(f)
		endif
	endif
endfunction

function sendMessage()
	msg = CreateNetworkMessage()
	r = opentoread("send.txt")
	str$ = readstring(r)
	
	addr$ = GetStringToken(str$,":",0) rem this is the client id that is from 
	msgg$ = getstringtoken(str$,":",1)
	
	closefile(r)
	AddNetworkMessageString(msg, msgg$)
	SendNetworkMessage(net, val(addr$), msg)
	deletenetworkmessage(msg)
	deletefile("send.txt")
endfunction 





